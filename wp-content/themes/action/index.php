<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<title>Action Blog Home</title>
</head>
<body>
	<?php get_header ();?>

	<!-- begin content -->

	

	

	<div class="jumbotron sobre-jumbo" style="background: url('<?php echo get_template_directory_uri();?>/assets/img/about/sobre.jpg'); background-repeat: no-repeat; background-size: cover">

	<div class="container">

		<h1>Blog</h1>
	</div>

</div>
<!-- begin breadcrumbs -->
<div class="container">
	<div class="col-md-12">
		<ol class="breadcrumb">
            <li><a href="<?php echo get_template_directory_uri();?>/blog-home.php">Blog</a></li>
            <li><a href="<?php echo get_template_directory_uri();?>/index.php">Home</a></li>
        </ol>
	</div>
</div>


<!-- conteudo -->
<article class="news">
		<div class="container">
			<div class="row">
				<div class="col-md-9 main">
					

		
					<div class="clearfix"></div>
					<ul class="pagination">
						<li><a href="<?php echo get_template_directory_uri();?>/">&lt;</a></li>
						<li><a href="<?php echo get_template_directory_uri();?>/">1</a></li>
						<li class="active"><a href="<?php echo get_template_directory_uri();?>/">2</a></li>
						<li><a href="<?php echo get_template_directory_uri();?>/">3</a></li>
						<li><a href="<?php echo get_template_directory_uri();?>/">4</a></li>
						<li><a href="<?php echo get_template_directory_uri();?>/">...</a></li>
						<li><a href="<?php echo get_template_directory_uri();?>/">12</a></li>
						<li><a href="<?php echo get_template_directory_uri();?>/">&gt;</a></li>
					</ul>
				</div>
				
				
				<div class="col-md-3">
					<div class="sidebar sidebar-right">

					<?php get_search_form(); ?>
						

	
						<h5>Categorias</h5>
						<ul class="categories">
						  <?php 
							$args = array(
							'orderby' => 'name',
							'order' => 'ASC',
							'hide_empty'=> 0
							);
							$categories = get_categories($args);
							foreach ($categories as $cat) {

							?>
							<li><a href="<?=get_category_link($cat->cat_ID);?>"><i class="icon-plus"></i> <?=$cat->name;?> <span class="label label-default pull-right"><?=$cat->category_count;?></span></a></li>
							<?php
							    }
							?>

						</ul>
					</div>
					
			</div>
		</div>
	</article>
<!-- fim do conteudo -->



	<!-- end content -->




	

	<?php get_footer ();?>
</body>
</html>