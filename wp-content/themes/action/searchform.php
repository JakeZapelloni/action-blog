<form action="<?php bloginfo('url'); ?>" method="get" data-ajax="1" class="search">
	<div class="form-group">
		<label for="foot-news-email" class="sr-only">Procure aqui...</label>
		<input type="text" class="form-control" value="<?php the_search_query(); ?>" name="foot-search" placeholder="Search here..." />
	</div>
	<button class="btn btn-search" type="submit"><i class="icon-search"></i></button>
</form>