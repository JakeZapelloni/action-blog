<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'actionblog');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'actionblog');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'action');

/** Nome do host do MySQL */
define('DB_HOST', 'actionblog.mysql.dbaas.com.br');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>p!/:,swj~m&%DtlvdC f-@e)W~#ivP00s{:Oy|k%mHwD/Ab^c5cu|#<YOT7glZz');
define('SECURE_AUTH_KEY',  '_n:X4-tM`h/%,[J0r97kuC0F2DS}[^FNvgR9@TKECP3Br)Wgla+up{+P{V9*OH$v');
define('LOGGED_IN_KEY',    'tu(W|wOk36@{c4PM7i32<55`u50+ChdSg=723l9AaH_>eDmF_l5pn){4*/~;&(JY');
define('NONCE_KEY',        'D)mj>d7bN|2`)E*y}BPY`spyOIx,o5.*&]Kfg[,?v4S_IevMO1{>&vyX$|h$Zw.p');
define('AUTH_SALT',        '6;9%Yy )&Hm_On$6-JK{=I(`iXbWN1Of%q&d.u&}fx!xxpIwKB@OMmD[DbeD;iY^');
define('SECURE_AUTH_SALT', '!6t38&(X@BrmbNVA4tCV1mFzRd;T2Z=[N%R#Quw^NlTvVGB(|!qhk;c|/PWhKXZH');
define('LOGGED_IN_SALT',   '#YBUkF$h{iqHzJ?Ry64:1qfIFy`.?y/`Jvxv%<YhJs^2hpB]Vb4Q/XC4>j:lm`Ti');
define('NONCE_SALT',       '_OK>s~,EMLA$LTK2M`AFT]joa2mG4oL;#,+!u??t@!%r=](Eq+rCt,R.oK6gU~56');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
