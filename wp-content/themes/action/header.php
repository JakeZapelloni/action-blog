<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8"/>
	<title>Action 360 | Treinamento Funcional e Pilates</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800%7CRaleway:400,100,200,300,500,600,800,700,900" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/vendors/fontello.css" />	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/vendors/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/js/vendors/owl-carousel/owl.carousel.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/js/vendors/owl-carousel/owl.theme.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/js/vendors/revolution/css/settings.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/js/vendors/malihu/jquery.mCustomScrollbar.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/styles.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/colors/red.css" />
	<link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/imgapple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/imgapple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/imgapple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/imgapple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/imgapple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="assets/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="assets/img/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>        
		<![endif]-->

		<!-- Media queries -->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- begin Header -->
		<header>
			<div class="topbar">
				<div class="container">
					<p class="date">Venha treinar na Action360º <strong>#SoActionÉAction</strong></p>
					<ul class="social">
						<li><a href="https://www.facebook.com/StudioAction360/" target="blank"><i class="icon-facebook"></i></a></li>
						<li><a href="https://www.instagram.com/action_360/" target="blank"><i class="icon-instagram"></i></a></li>
					</ul>
				</div>
			</div>
			<!-- begin Navbar -->
			<nav class="navbar" role="navigation">
				<div class="container">
					<!-- menu toggle for mobile-->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/logo.png" alt="Logo Action 360"></a>
					</div>

					<!-- menu collapse -->
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav menu-effect">	
							<li>
								<a href="#" alt="Logo Action 360" 
									class= "dropdown-toggle" data-toggle="dropdown"><span class="item-text">Conheça <span><span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Sobre a Action360º</a></li>
										<li><a href="#">Pilates</a></li>
										<li><a href="#">Treinamento Funcional</a></li>
									</ul>
								</li>
								<li><a href="about-us.php"><span class="item-text">Planos</span></a></li>
								<li>					
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="item-text">Unidades<span> <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Aclimação</a></li>
										<li><a href="#">Campo Belo</a></li>
										<li><a href="#">Ipiranga</a></li>
										<li><a href="#">Jardim Paulista</a></li>
										<li><a href="#">Moema Índios</a></li>
										<li><a href="#">Moema Pássaros</a></li>
										<li><a href="#">Paraíso</a></li>
										<li><a href="#">Perdizes</a></li>
										<li><a href="#">Pinheiros</a></li>
										<li><a href="#">Saúde</a></li>
										<li><a href="#">Tatuapé</a></li>
									</ul>
								</li>
								<li><a href="#"><span class="item-text">Franquias<span></a></li>
								<li>
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="item-text">Blog<span> <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#"><span class="item-text">Notícias<span></a></li>
										<li><a href="#>"><span class="item-text">Eventos<span></a></li>
										<li><a href="#"><span class="item-text">Categoria 3<span></a></li>						
									</ul>
								</li>
								<li><a href="fale-conosco.php"><span class="item-text">Contato<span></a></li>
								<li><a href="cursos.php"><span class="item-text">Cursos<span></a></li>

							</ul>
						</div><!--/.nav-collapse -->
					</div>
				</nav>

			</header>
			<!-- end Header -->