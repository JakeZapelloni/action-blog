<!-- begin Footer -->
			<footer>
				<div class="hours">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<div class="map" id="map"></div>
							</div>
							<div class="col-md-4 opening-hours">
								<h3>Horário de Funcionamento</h3>
								<ul>
									<li><p>Aclimação - 8h às 22h</p></li>
									<li><p>Campo Belo - 8h às 22h</p></li>
									<li><p>Ipiranga - 8h às 22h</p></li>
									<li><p>Jardim Paulista - 8h às 22h</p></li>
									<li><p>Moema Índios - 8h às 22h</p></li>
									<li><p>Moema Pássaros - 8h às 22h</p></li>
									<li><p>Perdizes - 8h às 22h</p></li>
									<li><p>Pinheiros - 8h às 22h</p></li>
									<li><p>Saúde - 8h às 22h</p></li>
									<li><p>Tatuapé - 8h às 22h</p></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="info">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-4 col-xs-12 siga">
								
								<ul class="classes-list">
									<li><img src="<?php echo get_template_directory_uri();?>/assets/img/logo_action_branco.png" alt="Logo Action 360" alt="Logo Action 360º"></li>
								</ul>
								
								<div id="social">
									<ul class="social">
										<li><a  href="https://www.facebook.com/StudioAction360/" target="blank"><i class="icon-facebook"></i></a></li>
										<li><a href="https://www.instagram.com/action_360/" target="blank"><i class="icon-instagram"></i></a></li>
									</ul>
								</div>
							</div>

							<div class="col-md-2 col-sm-4 col-xs-12">
								<h4>UNIDADES</h4>
								<ul class="classes-list">
									<li><a href=""></i>Aclimação</a></li>
									<li><a href="#"></i>Campo Belo</a></li>
									<li><a href="#"></i>Ipiranga</a></li>
									<li><a href="#"></i>Jardim Paulista</a></li>
									<li><a href="#"></i>Moema Índios</a></li>
									<li><a href="#"></i>Moema Pássaros</a></li>
									<li><a href="#"></i>Perdizes</a></li>
									<li><a href="#"></i>Pinheiros</a></li>
									<li><a href="#"></i>Saúde</a></li>
									<li><a href="#"></i>Tatuapé</a></li>


								</ul>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-12">
								<h4>ACTION 360º</h4>
								<ul class="classes-list">
									<li><a href="#"</i>Home</a></li>
									<li><a href="#"></i>Action 360º</a></li>
									<li><a href="#"></i>Pilates</a></li>
									<li><a href="#"></i>Funcional</a></li>
									<li><a href="#"></i>Franquia</a></li>
									<li><a href="#"></i>Blog</a>&nbsp;<span class="label label-default"> Novidades</span></li>
									<li><a href="#"></i>Fale Conosco</a></li>
								</ul>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-12">
								<h4>SOBRE</h4>
								<p class="text-footer">A ACTION 360º é um sistema de franchising especializado no mercado Fitness com estúdios focados em aulas para treinamento funcional e pilates.</p>
								<p class="text-footer"> Visando atender uma classe mais exigente, que busca treinamento e acompanhamento personalizado, a ideia surgiu ao ouvir inúmeras reclamações de alunos de outras unidades de negócio.</p>
								
							</div>
						</div>
					</div>
				</div>

				<div class="copy">
					<div class="container">
						<p class="top"><a href="#">Topo <i class="icon-up-thin"></i></a></p>
						<p>&copy; Action360º - 2016 - Todos os direitos reservados. |
							Desenvolvido por <a href="http://ziriga.com.br" target="blank">Agência Zíriga</a></p>
						</div>
					</div>

				</footer>
				<!-- end Footer --> 


				


				<!-- ******************************************************************** -->
				<!-- ************************* Javascript Files ************************* -->
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/jquery/jquery-1.11.1.min.js"></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/respond.min.js"></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/bootstrap.min.js" ></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/owl-carousel/owl.carousel.js"></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/revolution/js/jquery.themepunch.plugins.min.js"></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/malihu/jquery.mCustomScrollbar.concat.min.js"></script>

				<!-- Placeholder.js -->
				<!--[if lte IE 9]> <script src="js/vendors/placeholder.js" ></script> <script>Placeholder.init();</script> <![endif]-->
				 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMJO7dTY26UajJjPVEwaSG3v1ehMF_324"
  type="text/javascript"></script>
	
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/gmaps.js" ></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/bootstrap-slider.js" ></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/vendors/jquery/jquery.nav.js" ></script>
				<script src="<?php echo get_template_directory_uri();?>/assets/js/script.js"  ></script>

				<!-- API do google maps  -->
				
				<!-- *********************** End Javascript Files *********************** -->
				<!-- ******************************************************************** -->
				<?php wp_footer(); ?>

			</body>
			</html>